#ifndef __ADM3A_H__
#define __ADM3A_H__

#include <GL/gl.h>

typedef struct {
	GLuint		tex;
	int*		tex_data;

	int		lines;
	int		columns;
	int		cursor_x;
	int		cursor_y;
	unsigned char*	text;

	int		tmp;
	int		state;

	/* keyboard */
	unsigned char	keyboard_locked;
	unsigned char	key;

	/* configuration */
	unsigned char	auto_lf;

	/* I/O functions */
	void		(*rx)(unsigned char c);
} ADM3A;

#define	NUL		0x00
#define	SOH		0x01
#define	STX		0x02
#define	ETX		0x03
#define	EOT		0x04
#define	ENQ		0x05
#define	ACK		0x06
#define	BEL		0x07
#define	BS		0x08
#define	HT		0x09
#define	LF		0x0A
#define	VT		0x0B
#define	FF		0x0C
#define	CR		0x0D
#define	SO		0x0E
#define	SI		0x0F
#define	DLE		0x10
#define	DC1		0x11
#define	DC2		0x12
#define	DC3		0x13
#define	DC4		0x14
#define	NAK		0x15
#define	SYN		0x16
#define	ETB		0x17
#define	CAN		0x18
#define	EM		0x19
#define	SUB		0x1A
#define	ESC		0x1B
#define	FS		0x1C
#define	GS		0x1D
#define	RS		0x1E
#define	US		0x1F
#define	DEL		0x7F
#define	IND		0x84
#define	NEL		0x85
#define	HTS		0x88
#define	RI		0x8D
#define	SS2		0x8E
#define	SS3		0x8F
#define	DCS		0x90
#define	CSI		0x9B
#define	ST		0x9C

#define	ADM3A_KEY_RUB	0x7F
#define	ADM3A_KEY_BREAK	0xFE
#define	ADM3A_KEY_CLEAR	0xFD
#define	ADM3A_KEY_HERE	0xFC

/* functions */
void ADM3AInit(ADM3A* adm);
void ADM3ADestroy(ADM3A* adm);
void ADM3AReceive(ADM3A* adm, unsigned char c);
void ADM3AReceiveText(ADM3A* adm, char* c);
void ADM3AKeyDown(ADM3A* adm, unsigned char c);
void ADM3AKeyUp(ADM3A* adm, unsigned char c);
void ADM3ADraw(ADM3A* adm);

/* private */
extern const unsigned char adm3a_font_uc[64 * 8];
extern const unsigned char adm3a_font_lc[64 * 8];
extern const unsigned char adm3a_font[96 * 9];

int* ADM3ACreateTextureSheet(void);
void ADM3AScrollUp(ADM3A* adm);
void ADM3AWrite(ADM3A* adm, unsigned char c);
void ADM3AWriteChar(ADM3A* adm, unsigned char c);
void ADM3ACursorLeft(ADM3A* adm);
void ADM3ACursorRight(ADM3A* adm);
void ADM3ACursorUp(ADM3A* adm);
void ADM3ACursorDown(ADM3A* adm);
void ADM3ASetCursor(ADM3A* adm, int line, int column);
void ADM3ACarriageReturn(ADM3A* adm);
void ADM3ALinefeed(ADM3A* adm);
void ADM3AUnlockKeyboard(ADM3A* adm);
void ADM3ALockKeyboard(ADM3A* adm);
void ADM3AClearScreen(ADM3A* adm);
void ADM3AHomeCursor(ADM3A* adm);
void ADM3AProcess(ADM3A* adm, unsigned char c);

#endif
