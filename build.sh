#!/bin/sh
# gcc -fsanitize=address -g -std=gnu99 -O3 -o vt240 -Iinclude src/*.c -lGL -lGLU -lglut -lm
gcc -Wall -g -std=gnu99 -O3 -o adm-3a -DGL_GLEXT_PROTOTYPES -Iinclude src/*.c -lGL -lGLU -lglut -lm
