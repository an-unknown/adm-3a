#ifdef _WIN32
#include <windows.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <GL/gl.h>
#include <GL/glext.h>
#include <GL/glut.h>

#include <math.h>

#include "types.h"
#include "adm3a.h"
#include "telnet.h"

#define M_PI			3.14159265358979323846

#define	SCREEN_WIDTH		560
#define	SCREEN_HEIGHT		432

#define	BLUR_WIDTH		(SCREEN_WIDTH / 1)
#define	BLUR_HEIGHT		(SCREEN_HEIGHT / 1)

#if 0
#define	BLUR_INTENSITY		0.25f

#define	BLUR_ITERATIONS		5
#define	BLUR_FRACTION		200.0f

#define	BLUR_INTENSITY2		0.1f
#define	BLUR_ITERATIONS2	5
#define	BLUR_FRACTION2		300.0f
#endif

#define	BLUR_INTENSITY		0.2f

#define	BLUR_ITERATIONS		5
#define	BLUR_FRACTION		200.0f

#define	BLUR_INTENSITY2		0.1f
#define	BLUR_ITERATIONS2	5
#define	BLUR_FRACTION2		300.0f

// #define	USE_FLICKER
#define	FLICKER_RATE		30.0
#define	FLICKER_TIME		(1.0f / (2.0f * FLICKER_RATE))

static bool enable_glow = true;
static bool use_telnet = false;

static ADM3A adm;
static TELNET telnet;

static long time = 0;

static GLuint scanlines;

static char scanline_tex[4 * 4] = {
	0x00, 0x00, 0x00, 0x00,		0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0xFF,		0x00, 0x00, 0x00, 0xFF
};

static GLuint blur_fb[2];
static GLuint blur_tex[2];

float flicker_time = 0;

#ifdef _WIN32
PFNGLGENFRAMEBUFFERSPROC	glGenFramebuffers;
PFNGLBINDFRAMEBUFFERPROC	glBindFramebuffer;
PFNGLFRAMEBUFFERTEXTUREPROC	glFramebufferTexture;
PFNGLCHECKFRAMEBUFFERSTATUSPROC	glCheckFramebufferStatus;

static void load_gl_extensions(void)
{
	glGenFramebuffers = (PFNGLGENFRAMEBUFFERSPROC)wglGetProcAddress("glGenFramebuffers");
	glBindFramebuffer = (PFNGLBINDFRAMEBUFFERPROC)wglGetProcAddress("glBindFramebuffer");
	glFramebufferTexture = (PFNGLFRAMEBUFFERTEXTUREPROC)wglGetProcAddress("glFramebufferTexture");
	glCheckFramebufferStatus = (PFNGLCHECKFRAMEBUFFERSTATUSPROC)wglGetProcAddress("glCheckFramebufferStatus");
}
#endif

void process(void)
{
	long now = glutGet(GLUT_ELAPSED_TIME);

	float dt = (now - time) / 1000.0f;

	flicker_time += dt;
	if(flicker_time >= 2 * FLICKER_TIME) {
		while(flicker_time >= FLICKER_TIME) {
			flicker_time -= FLICKER_TIME;
		}
	}

	if(use_telnet) {
		TELNETPoll(&telnet);
	}

	time = now;
}

void kb_func(unsigned char key, int x, int y)
{
	ADM3AKeyDown(&adm, key);
}

void kb_up_func(unsigned char key, int x, int y)
{
	ADM3AKeyUp(&adm, key);
}

void special_func(int key, int x, int y)
{
	switch(key) {
		case GLUT_KEY_F1:
		case GLUT_KEY_F2:
		case GLUT_KEY_F3:
		case GLUT_KEY_F4:
		case GLUT_KEY_F5:
		case GLUT_KEY_F6:
		case GLUT_KEY_F7:
		case GLUT_KEY_F8:
		case GLUT_KEY_F9:
		case GLUT_KEY_F10:
		case GLUT_KEY_F11:
		case GLUT_KEY_F12:
			break;
		case GLUT_KEY_LEFT:
			ADM3AKeyDown(&adm, BS);
			break;
		case GLUT_KEY_UP:
			ADM3AKeyDown(&adm, VT);
			break;
		case GLUT_KEY_RIGHT:
			ADM3AKeyDown(&adm, FF);
			break;
		case GLUT_KEY_DOWN:
			ADM3AKeyDown(&adm, LF);
			break;
		case GLUT_KEY_PAGE_UP:
		case GLUT_KEY_PAGE_DOWN:
			break;
		case GLUT_KEY_HOME:
			ADM3AKeyDown(&adm, RS);
			break;
		case GLUT_KEY_END:
		case GLUT_KEY_INSERT:
			break;
	}
}

void special_up_func(int key, int x, int y)
{
	switch(key) {
		case GLUT_KEY_F1:
		case GLUT_KEY_F2:
		case GLUT_KEY_F3:
		case GLUT_KEY_F4:
		case GLUT_KEY_F5:
		case GLUT_KEY_F6:
		case GLUT_KEY_F7:
		case GLUT_KEY_F8:
		case GLUT_KEY_F9:
		case GLUT_KEY_F10:
		case GLUT_KEY_F11:
		case GLUT_KEY_F12:
			break;
		case GLUT_KEY_LEFT:
			ADM3AKeyUp(&adm, BS);
			break;
		case GLUT_KEY_UP:
			ADM3AKeyUp(&adm, VT);
			break;
		case GLUT_KEY_RIGHT:
			ADM3AKeyUp(&adm, FF);
			break;
		case GLUT_KEY_DOWN:
			ADM3AKeyUp(&adm, LF);
			break;
		case GLUT_KEY_PAGE_UP:
		case GLUT_KEY_PAGE_DOWN:
			break;
		case GLUT_KEY_HOME:
			ADM3AKeyUp(&adm, RS);
			break;
		case GLUT_KEY_END:
		case GLUT_KEY_INSERT:
			break;
	}
}

void print_ch(unsigned char c)
{
	// ignore XON/XOFF
	if(c == DC1 || c == DC3) {
		return;
	}

	putc(c, stdout);
	fflush(stdout);

	ADM3AReceive(&adm, c);
}

void render_fullscreen_quad_offset(float x, float y)
{
	glBegin(GL_QUADS);

	glTexCoord2f(0, 0);
	glVertex2f(-1 + x, -1 + y);

	glTexCoord2f(1, 0);
	glVertex2f(1 + x, -1 + y);

	glTexCoord2f(1, 1);
	glVertex2f(1 + x, 1 + y);

	glTexCoord2f(0, 1);
	glVertex2f(-1 + x, 1 + y);

	glEnd();
}

void render_scanlines(GLfloat vp[4], float intensity)
{
	glColor4f(1, 1, 1, intensity);
	glBindTexture(GL_TEXTURE_2D, scanlines);
	glBegin(GL_QUADS);

	glTexCoord2f(0, 0);
	glVertex2f(0, 0);

	glTexCoord2f(vp[2] / 2, 0);
	glVertex2f(vp[2], 0);

	glTexCoord2f(vp[2] / 2, vp[3] / 2);
	glVertex2f(vp[2], vp[3]);

	glTexCoord2f(0, vp[3] / 2);
	glVertex2f(0, vp[3]);

	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
}

void render_glow(GLfloat vp[4])
{
	float aspect = (float) BLUR_WIDTH / (float) BLUR_HEIGHT;

	// Pass 0: render ADM-3A screen to texture
	glBindFramebuffer(GL_FRAMEBUFFER, blur_fb[0]);
	glClear(GL_COLOR_BUFFER_BIT);
	glViewport(0, 0, BLUR_WIDTH, BLUR_HEIGHT);

	ADM3ADraw(&adm);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();

	glBlendFunc(GL_SRC_ALPHA, GL_ONE);

	// Pass 1: blur (to texture)
	glBindFramebuffer(GL_FRAMEBUFFER, blur_fb[1]);
	glClear(GL_COLOR_BUFFER_BIT);

	glBindTexture(GL_TEXTURE_2D, blur_tex[0]);
	float intensity = BLUR_INTENSITY / BLUR_ITERATIONS;
	glColor4f(1, 1, 1, intensity);
	for(int i = 0; i < BLUR_ITERATIONS; i++) {
		for(int j = 0; j < BLUR_ITERATIONS; j++) {
			float x = ((float)i - (BLUR_ITERATIONS / 2)) / BLUR_FRACTION;
			float y = ((float)j - (BLUR_ITERATIONS / 2)) / BLUR_FRACTION;
			render_fullscreen_quad_offset(x, y * aspect);
		}
	}

	// Pass 2: blur (to texture)
	glBindFramebuffer(GL_FRAMEBUFFER, blur_fb[0]);
	glClear(GL_COLOR_BUFFER_BIT);

	glBlendFunc(GL_ONE, GL_ONE);
	glBindTexture(GL_TEXTURE_2D, blur_tex[1]);
	intensity = BLUR_INTENSITY / BLUR_ITERATIONS2;
	glColor4f(intensity, intensity, intensity, 1);
	for(int i = 0; i < BLUR_ITERATIONS2; i++) {
		for(int j = 0; j < BLUR_ITERATIONS2; j++) {
			float x = ((float)i - (BLUR_ITERATIONS2 / 2)) / BLUR_FRACTION2;
			float y = ((float)j - (BLUR_ITERATIONS2 / 2)) / BLUR_FRACTION2;
			render_fullscreen_quad_offset(x, y * aspect);
		}
	}

	// Pass 3: blur (to screen)
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBlendFunc(GL_ONE, GL_ONE);
	glBindTexture(GL_TEXTURE_2D, blur_tex[0]);
	glViewport(vp[0], vp[1], vp[2], vp[3]);
	intensity = BLUR_INTENSITY2 / BLUR_ITERATIONS2;
	glColor4f(intensity, intensity, intensity, 1);
	for(int i = 0; i < BLUR_ITERATIONS2; i++) {
		for(int j = 0; j < BLUR_ITERATIONS2; j++) {
			float x = ((float)i - (BLUR_ITERATIONS2 / 2)) / BLUR_FRACTION2;
			float y = ((float)j - (BLUR_ITERATIONS2 / 2)) / BLUR_FRACTION2;
			render_fullscreen_quad_offset(x, y * aspect);
		}
	}

	glBindTexture(GL_TEXTURE_2D, 0);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
}

void display_func(void)
{
	process();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	GLfloat vp[4];
	glGetFloatv(GL_VIEWPORT, vp);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, SCREEN_WIDTH, SCREEN_HEIGHT, 0); // l/r/b/t

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// ADM-3A screen
	ADM3ADraw(&adm);

	// scanlines
	render_scanlines(vp, 0.5);

	// glow
	if(enable_glow) {
		render_glow(vp);
		render_scanlines(vp, 0.2);
	}

	// flicker
#ifdef USE_FLICKER
	if(flicker_time >= FLICKER_TIME) {
		glColor4f(0, 0, 0, 0.3f);
		glBegin(GL_QUADS);
		glVertex2f(0, 0);
		glVertex2f(0, vp[3]);
		glVertex2f(vp[2], vp[3]);
		glVertex2f(vp[2], 0);
		glEnd();
	}
#endif

	glutSwapBuffers();
}

static void telnet_rx(unsigned char c)
{
	ADM3AReceive(&adm, c);
}

static void telnet_tx(unsigned char c)
{
	TELNETSend(&telnet, c);
}

int main(int argc, char** argv)
{
	const char* self = *argv;
	char* modestring = NULL;
	int use_game_mode = 0;
	argc--;
	argv++;
	if(argc > 1 && !strcmp(argv[0], "-f")) {
		modestring = argv[1];
		argc -= 2;
		argv += 2;
	}
	if(argc != 0 && argc != 2) {
		printf("Usage: %s [-f modestring] [hostname port]\n", self);
		exit(0);
	}

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
	if(modestring) {
		glutGameModeString(modestring);
		int possible = glutGameModeGet(GLUT_GAME_MODE_POSSIBLE);
		if(possible) {
			glutEnterGameMode();
			use_game_mode = 1;
		}
	}

	if(!use_game_mode) {
		glutInitWindowSize(SCREEN_WIDTH, SCREEN_HEIGHT);
		glutInitWindowPosition(100, 100);
		glutCreateWindow("ADM-3A");
	}

	const unsigned char* gl_vendor = glGetString(GL_VENDOR);
	const unsigned char* gl_renderer = glGetString(GL_RENDERER);
	const unsigned char* gl_version = glGetString(GL_VERSION);
	const unsigned char* gl_glsl_version = glGetString(GL_SHADING_LANGUAGE_VERSION);

	printf("GL Vendor:    %s\n", gl_vendor);
	printf("GL Renderer:  %s\n", gl_renderer);
	printf("GL Version:   %s\n", gl_version);
	printf("GLSL Version: %s\n", gl_glsl_version);

	printf("using depth buffer with %d bit\n", glutGet(GLUT_WINDOW_DEPTH_SIZE));

#ifdef _WIN32
	load_gl_extensions();
#endif

	glutDisplayFunc(display_func);
	glutIdleFunc(display_func);

	glutKeyboardFunc(kb_func);
	glutKeyboardUpFunc(kb_up_func);
	glutSpecialFunc(special_func);
	glutSpecialUpFunc(special_up_func);

	glutIgnoreKeyRepeat(1);

	ADM3AInit(&adm);
	adm.rx = print_ch;

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0, 0.0, 0.0, 0.0);

	// create scanline texture
	glGenTextures(1, &scanlines);
	glBindTexture(GL_TEXTURE_2D, scanlines);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 2, 2, 0, GL_RGBA, GL_UNSIGNED_BYTE, (GLvoid*) scanline_tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	// create blur render buffer and texture
	glGenFramebuffers(2, blur_fb);
	glGenTextures(2, blur_tex);

	glBindTexture(GL_TEXTURE_2D, blur_tex[0]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, BLUR_WIDTH, BLUR_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glBindTexture(GL_TEXTURE_2D, blur_tex[1]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, BLUR_WIDTH, BLUR_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glBindTexture(GL_TEXTURE_2D, 0);

	glBindFramebuffer(GL_FRAMEBUFFER, blur_fb[0]);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, blur_tex[0], 0);

	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		printf("error configuring framebuffer\n");
		enable_glow = 0;
	}

	if(enable_glow) {
		glBindFramebuffer(GL_FRAMEBUFFER, blur_fb[1]);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, blur_tex[1], 0);

		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
			printf("error configuring framebuffer\n");
			enable_glow = 0;
		}
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	if(argc == 2) {
		const char* hostname = argv[0];
		const int port = atoi(argv[1]);
		char buf[256];
		use_telnet = true;

		ADM3AReceiveText(&adm, "\x1a\x1b=  ");
		sprintf(buf, "Connecting to %s on port %d\r\n", hostname, port);
		ADM3AReceiveText(&adm, buf);

		TELNETInit(&telnet);
		telnet.rx = telnet_rx;
		adm.rx = telnet_tx;

		TELNETConnect(&telnet, argv[0], atoi(argv[1]));
	}

	time = glutGet(GLUT_ELAPSED_TIME);
	glutMainLoop();

	return 0;
}
