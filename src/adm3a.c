#include <stdlib.h>
#include <string.h>
#include "adm3a.h"

#define	CHAR_WIDTH		5
#define	CHAR_HEIGHT		9

#define	CELL_WIDTH		(CHAR_WIDTH + 2)
#define	CELL_HEIGHT		(CHAR_HEIGHT + 2)

#define	WIDTH			256
#define	HEIGHT			256

#define CELLS_PER_LINE		(WIDTH / CELL_WIDTH)

#define	MIN(x, y)		((x) < (y) ? (x) : (y))

#define	FGCOLOR			0xFFFFFFFF

#if 0
#define	COLOR_R			0.69f
#define	COLOR_G			0.97f
#define	COLOR_B			1.0f
#endif

#define	COLOR_R			0.67f
#define	COLOR_G			0.94f
#define	COLOR_B			1.0f

static inline int ADM3AGetBit(const unsigned char* font, int c, int x, int y)
{
	int idx = c * CHAR_HEIGHT + y;
	char line = font[idx];
	int bit = ((line << x) & 0x10) != 0;
	return bit;
}

void ADM3AExpandCharacter(int* txtr, int x, int y, int w, int c, int fg, const unsigned char* font)
{
	int px, py;
	for(py = 0; py < CHAR_HEIGHT; py++) {
		for(px = 0; px < CHAR_WIDTH; px++) {
			int bit = ADM3AGetBit(font, c, px, py);
			txtr[(y + py) * w + x + px] = bit ? fg : 0;
		}
	}
}

int* ADM3ACreateTextureSheet(void)
{
	int i, x, y, px, py;
	int* txtr = (int*) malloc(WIDTH * HEIGHT * sizeof(int));
	memset(txtr, 0, WIDTH * HEIGHT * sizeof(int));

#if 0
	for(i = 0; i < 64; i++) {
		y = i / CELLS_PER_LINE;
		x = i % CELLS_PER_LINE;
		px = x * CELL_WIDTH;
		py = y * CELL_HEIGHT;
		ADM3AExpandCharacter(txtr, px + 1, py + 1, WIDTH, i, FGCOLOR, adm3a_font_uc);
	}

	for(i = 0; i < 32; i++) {
		int n = i + 0x40;
		y = n / CELLS_PER_LINE;
		x = n % CELLS_PER_LINE;
		px = x * CELL_WIDTH;
		py = y * CELL_HEIGHT;
		ADM3AExpandCharacter(txtr, px + 1, py + 1, WIDTH, i, FGCOLOR, adm3a_font_lc);
	}
#endif

	for(i = 0; i < 96; i++) {
		y = i / CELLS_PER_LINE;
		x = i % CELLS_PER_LINE;
		px = x * CELL_WIDTH;
		py = y * CELL_HEIGHT;
		ADM3AExpandCharacter(txtr, px + 1, py + 1, WIDTH, i, FGCOLOR, adm3a_font);
	}

	return txtr;
}

void ADM3AInit(ADM3A* adm)
{
	adm->columns = 80;
	adm->lines = 24;
	adm->text = (unsigned char*) malloc(adm->lines * adm->columns);
	memset(adm->text, 0, adm->lines * adm->columns);

	adm->cursor_x = 0;
	adm->cursor_y = 0;

	adm->tex_data = ADM3ACreateTextureSheet();

	glGenTextures(1, &adm->tex);
	glBindTexture(GL_TEXTURE_2D, adm->tex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, WIDTH, HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, (GLvoid*) adm->tex_data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glBindTexture(GL_TEXTURE_2D, 0);

	adm->state = 0;
	adm->keyboard_locked = 0;

	/* default configuration */
	adm->auto_lf = 1;
}

void ADM3ADestroy(ADM3A* adm)
{
	glDeleteTextures(1, &adm->tex);
	free(adm->text);
	free(adm->tex_data);
}

void ADM3AScrollUp(ADM3A* adm)
{
	int i;
	int start = adm->columns;
	int end = adm->lines * adm->columns;
	for(i = start; i < end; i++) {
		adm->text[i - adm->columns] = adm->text[i];
	}
	end = (adm->lines - 1) * adm->columns;
	memset(&adm->text[end], 0, adm->columns);
}

void ADM3AWrite(ADM3A* adm, unsigned char c)
{
	adm->text[adm->cursor_y * adm->columns + adm->cursor_x] = c;

	adm->cursor_x++;
	if(adm->cursor_x == adm->columns) {
		if(adm->auto_lf) {
			adm->cursor_x = 0;
			adm->cursor_y++;
			if(adm->cursor_y == adm->lines) {
				adm->cursor_y--;
				ADM3AScrollUp(adm);
			}
		} else {
			adm->cursor_x--;
		}
	}
}

void ADM3AWriteChar(ADM3A* adm, unsigned char c)
{
	if(c >= 0x20 && c < 0x60) {
		ADM3AWrite(adm, c - 0x20);
	} else if(c >= 0x60 && c < 0x80) {
		ADM3AWrite(adm, c - 0x60 + 0x40);
	}
}

void ADM3ACursorLeft(ADM3A* adm)
{
	if(adm->cursor_x > 0) {
		adm->cursor_x--;
	}
}

void ADM3ACursorRight(ADM3A* adm)
{
	adm->cursor_x++;
	if(adm->cursor_x == adm->columns) {
		adm->cursor_x = 0;
		ADM3ALinefeed(adm);
	}
}

void ADM3ACursorUp(ADM3A* adm)
{
	if(adm->cursor_y > 0) {
		adm->cursor_y--;
	}
}

void ADM3ACursorDown(ADM3A* adm)
{
	adm->cursor_y++;
	if(adm->cursor_y == adm->lines) {
		adm->cursor_y--;
	}
}

void ADM3ASetCursor(ADM3A* adm, int line, int column)
{
	adm->cursor_x = line;
	adm->cursor_y = column;

	if(adm->cursor_x < 0) {
		adm->cursor_x = 0;
	}

	if(adm->cursor_x >= adm->columns) {
		adm->cursor_x = adm->columns - 1;
	}

	if(adm->cursor_y < 0) {
		adm->cursor_y = 0;
	}

	if(adm->cursor_y >= adm->lines) {
		adm->cursor_y = adm->lines - 1;
	}
}

void ADM3ACarriageReturn(ADM3A* adm)
{
	adm->cursor_x = 0;
}

void ADM3ALinefeed(ADM3A* adm)
{
	adm->cursor_y++;
	if(adm->cursor_y == adm->lines) {
		adm->cursor_y--;
		ADM3AScrollUp(adm);
	}
}

void ADM3AUnlockKeyboard(ADM3A* adm)
{
	adm->keyboard_locked = 0;
}

void ADM3ALockKeyboard(ADM3A* adm)
{
	adm->keyboard_locked = 1;
}

void ADM3AClearScreen(ADM3A* adm)
{
	memset(adm->text, 0, adm->columns * adm->lines);
	adm->cursor_x = 0;
	adm->cursor_y = 0;
}

void ADM3AHomeCursor(ADM3A* adm)
{
	ADM3ASetCursor(adm, 0, 0);
}

#define	STATE_TEXT	0
#define	STATE_ESC	1
#define	STATE_LOAD_X	2
#define	STATE_LOAD_Y	3

void ADM3AProcess(ADM3A* adm, unsigned char c)
{
	switch(adm->state) {
		case STATE_TEXT:
			switch(c) {
				case ESC:
					adm->state = STATE_ESC;
					break;
				case BS:
					ADM3ACursorLeft(adm);
					break;
				case LF:
					ADM3ALinefeed(adm);
					break;
				case VT:
					ADM3ACursorUp(adm);
					break;
				case FF:
					ADM3ACursorRight(adm);
					break;
				case CR:
					ADM3ACarriageReturn(adm);
					break;
				case SO:
					ADM3AUnlockKeyboard(adm);
					break;
				case SI:
					ADM3ALockKeyboard(adm);
					break;
				case SUB:
					ADM3AClearScreen(adm);
					break;
				case RS:
					ADM3AHomeCursor(adm);
					break;
				default:
					ADM3AWriteChar(adm, c);
					break;
			}
			break;
		case STATE_ESC:
			switch(c) {
				case '=': /* load cursor */
					adm->state = STATE_LOAD_Y;
					break;
				default:
					adm->state = STATE_TEXT;
					break;
			}
			break;
		case STATE_LOAD_Y:
			if(c >= 0x20 && c <= 0x37) {
				adm->tmp = c - 0x20;
				adm->state = STATE_LOAD_X;
			} else {
				adm->state = STATE_TEXT;
			}
			break;
		case STATE_LOAD_X:
			if(c >= 0x20 && c <= 0x6F) {
				ADM3ASetCursor(adm, c - 0x20, adm->tmp);
			}
			adm->state = STATE_TEXT;
			break;
	}
}

void ADM3AReceive(ADM3A* adm, unsigned char c)
{
	ADM3AProcess(adm, c);
}

void ADM3AReceiveText(ADM3A* adm, char* s)
{
	for(; *s; s++) {
		ADM3AReceive(adm, *s);
	}
}

void ADM3AKeyDown(ADM3A* adm, unsigned char c)
{
	adm->key = c;
	adm->rx(c);
}

void ADM3AKeyUp(ADM3A* adm, unsigned char c)
{
	adm->key = 0xFF;
}

static inline void ADM3ADrawChar(ADM3A* adm, int ch, int posX, int posY)
{
	int py = ch / CELLS_PER_LINE;
	int px = ch % CELLS_PER_LINE;
	int startX = px * CELL_WIDTH + 1;
	int startY = py * CELL_HEIGHT + 1;
	int endX = startX + CHAR_WIDTH;
	int endY = startY + CHAR_HEIGHT;
	int x = posX * (CHAR_WIDTH + 2) + 1;
	int y = posY * (2 * CHAR_HEIGHT);

	glTexCoord2f((float) startX / (float) WIDTH, (float) startY / (float) HEIGHT);
	glVertex2f(x, y);
	glTexCoord2f((float) endX / (float) WIDTH, (float) startY / (float) HEIGHT);
	glVertex2f(x + CHAR_WIDTH, y);
	glTexCoord2f((float) endX / (float) WIDTH, (float) endY / (float) HEIGHT);
	glVertex2f(x + CHAR_WIDTH, y + 2 * CHAR_HEIGHT);
	glTexCoord2f((float) startX / (float) WIDTH, (float) endY / (float) HEIGHT);
	glVertex2f(x, y + 2 * CHAR_HEIGHT);
}

static inline void ADM3ADrawCursor(ADM3A* adm)
{
	int x = adm->cursor_x * (CHAR_WIDTH + 2) + 1;
	int y = adm->cursor_y * (2 * CHAR_HEIGHT);

	glBegin(GL_QUADS);
	glVertex2f(x - 1, y);
	glVertex2f(x - 1 + CHAR_WIDTH + 2, y);
	glVertex2f(x - 1 + CHAR_WIDTH + 2, y + 2 * CHAR_HEIGHT);
	glVertex2f(x - 1, y + 2 * CHAR_HEIGHT);
	glEnd();
}

void ADM3ADraw(ADM3A* adm)
{
	const int size = adm->lines * adm->columns;
	int posX = 0;
	int posY = 0;
	int i;
	unsigned char c;

	/* draw text */
	glBindTexture(GL_TEXTURE_2D, adm->tex);
	glBegin(GL_QUADS);

	glColor4f(COLOR_R, COLOR_G, COLOR_B, 1.0f);
	for(i = 0; i < size; i++) {
		c = adm->text[i];
		if(posX != adm->cursor_x || posY != adm->cursor_y) {
			ADM3ADrawChar(adm, c, posX, posY);
		}
		posX++;
		if(posX == adm->columns) {
			posX = 0;
			posY++;
		}
	}

	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

	/* draw cursor */
	ADM3ADrawCursor(adm);

	/* inverse character at cursor position */
	glBindTexture(GL_TEXTURE_2D, adm->tex);
	glBegin(GL_QUADS);

	glColor4f(0, 0, 0, 1);
	posX = adm->cursor_x;
	posY = adm->cursor_y;
	c = adm->text[posY * adm->columns + posX];
	ADM3ADrawChar(adm, c, posX, posY);

	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);
}
